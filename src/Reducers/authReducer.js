import {toast} from "react-toastify";

const authReducer = (state={}, action) => {
	switch(action.type){
		case "SIGN_IN": 
		return state;
		case "SIGN_IN_ERR":
		toast.error("User not found! Please check email and password correctly.");
		return state;
		case "SIGN_OUT":  
		return state;
		case "SIGN_UP":
		toast("Thank you for joining us!");
		return state;
		case "SIGN_UP_ERR":
		toast.error("Email Already Exists!");
		return state;
		default: return state;
	}
}

export default authReducer;