import { combineReducers } from "redux";
import {firebaseReducer} from "react-redux-firebase";
import {firestoreReducer} from "redux-firestore";
// import taskReducer from "./taskReducer";
// import authReducer from "./authReducer";
// import ProfileReducer from "./ProfileReducer";

const rootReducer = combineReducers({
firebase: firebaseReducer,
firestore: firestoreReducer,
// task: taskReducer,
// auth: authReducer,
// profile: ProfileReducer
});


export default rootReducer;