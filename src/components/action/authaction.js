
export const signin = creds => {
	return (dispatch, getState, {getFirebase}) => {
		const firebase = getFirebase();
		firebase
		.auth()
		.signInWithEmailAndPassword(creds.email, creds.password)
		.then(() => {
			//dispatch({ type: "SIGN_IN"});
			alert("Sign in");
		})
		.catch(err => {
			//dispatch({ type: "SIGN_IN_ERR"}, err);
			alert("Error Sign in");
		});
	};
};


export const signOut = () => {
	return (dispatch, getState, {getFirebase}) => {
		const firebase = getFirebase();
		firebase
		.auth()
		.signOut()
		.then(() => {
			dispatch({ type: "SIGN_OUT"});
		});
	};
};

export const signup = creds => {
	return (dispatch, getState, {getFirebase}) => {
		const firebase = getFirebase();
		firebase
		.auth()
		.createUserWithEmailAndPassword(creds.email, creds.password)
		.then(() => {
			alert("REgister");
		})
		.catch(err => {
			alert("error reg");
		});
	};
};