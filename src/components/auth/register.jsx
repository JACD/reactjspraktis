import React, {Component} from "react";
import {signup} from "../action/authaction";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom"; 

import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

class Signup extends Component {
    state = {
		email: "",
		password: "",
	};

handleChange = (e) => {
this.setState({
	[e.target.id]: e.target.value,
});
};

handleSubmit = (e) => {
e.preventDefault()
console.log(this.state);
this.props.signup(this.state);
};

  render () {
    const {uid} = this.props;
	if(uid) return <Redirect to="/profile" />

	return ( 
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="7" xl="6">
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm onSubmit = {this.handleSubmit}>
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p> 
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>@</CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="text" placeholder="Email" autoComplete="email" id="email" onChange = {this.handleChange} required/>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="password" placeholder="Password" autoComplete="new-password" id="password" onChange = {this.handleChange} required/>
                  </CInputGroup> 
                  <h5 class="mt-3 fade show">Already a Member? <a href="./../#/login">Login</a></h5>
                  <CButton type="submit" color="success" block>Create Account</CButton>
                </CForm>
              </CCardBody> 
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div> 
    ); 
}
}

const mapStateToProps = (state) => {
	console.log(state);
	const uid = state.firebase.auth.uid;
	return {
	uid: uid
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		signup: (creds) => dispatch(signup(creds))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
