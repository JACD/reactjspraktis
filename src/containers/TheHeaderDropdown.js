import React from 'react' 
import {signOut} from "../components/action/authaction";
import {connect} from "react-redux"; 
import {Link} from "react-router-dom";
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const TheHeaderDropdown = ({signOut}) => {
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/6.jpg'}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem>
          <CIcon name="cil-user" className="mfe-2" />Profile
        </CDropdownItem> 
        <CDropdownItem divider />
        <CDropdownItem>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          <Link to="/signin" className="nav-link" onClick={signOut}>
LogOut
</Link>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}
 
const mapStateToProps = state => {
	const uid = state.firebase.auth.uid;
	return {
		uid: uid
	}
}

const mapDispatchToProps = (dispatch) =>{
	return {
	signOut: () => dispatch(signOut())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(TheHeaderDropdown);