import React from 'react'
import {connect} from "react-redux";
import {Redirect} from "react-router-dom"; 
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'

const TheLayout = ({uid}) => {
  if(uid){
  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
        <TheContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  ) 
} 
else 
 {
    return <Redirect to="/login"/>
  } 
}

const mapStateToProps = state => {
	const uid = state.firebase.auth.uid;
	return {
		uid: uid
	}
}
 

export default connect(mapStateToProps)(TheLayout);
